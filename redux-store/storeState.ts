import { IPostsState } from './posts/state';
import { IInsightsState } from './insights/state';
/* new-imported-state-goes-here */

export interface IStoreState {
  readonly posts: IPostsState;
  readonly insights: IInsightsState;
	/* new-imported-state-key-goes-here */
}
