import postsSaga from './posts/sagas';
import insights from './insights/sagas';
/* new-imported-saga-goes-here */

export default [
  postsSaga,
  insights,
/* new-imported-saga-element-goes-here */
];
