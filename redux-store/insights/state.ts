export type InsightsErrable =
  | '__errable__' // Remove this. It's just a placeholder
  /* new-errable-goes-here */;

export type InsightsBooleanable =
  | '__booleanable__' // Remove this. It's just a placeholder
  /* new-booleanable-goes-here */;

export type InsightsSuccessible =
  | '__successible__' // Remove this. It's just a placeholder
  /* new-successible-goes-here */;

export interface IInsightsState{

  //#region Doables
  readonly errable?: { [key in InsightsErrable]?: string };
  readonly booleanable?: { [key in InsightsBooleanable]?: boolean };
  readonly successible?: { [key in InsightsSuccessible]?: string };
  //#endregion
}
