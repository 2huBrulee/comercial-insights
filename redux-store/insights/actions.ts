import { createAction } from 'redux-actions';
import {
  DEFAULT_ACTION,
  RESET_INSIGHTS_DOABLES,
  TOGGLE_INSIGHTS_BOOLEANABLE_STATE,
  TOGGLE_INSIGHTS_ERRABLE_STATE,
  TOGGLE_INSIGHTS_SUCCESSIBLE_STATE,
  /* new-constant-import-goes-here */
} from './constants';
import {
  IInsightsState,
  InsightsErrable,
  InsightsBooleanable,
  InsightsSuccessible,
} from './state';

export const defaultAction = createAction<IInsightsState>(DEFAULT_ACTION, () => ({
  errable: { __errable__: null },
  booleanable: { __booleanable__: true },
  successible: { __successible__: 'Successfully initialized!' },
}));

//#region Doables
export const resetInsightsDoables = createAction<IInsightsState>(RESET_INSIGHTS_DOABLES, () => ({
  errable: {},
  booleanable: {},
  successible: {},
}));

export const toggleInsightsBooleanableState = createAction<
  IInsightsState,
  { [key in InsightsBooleanable]?: boolean }
>(TOGGLE_INSIGHTS_BOOLEANABLE_STATE, key => ({
  booleanable: key,
}));

export const toggleInsightsErrableState = createAction<IInsightsState, { [key in InsightsErrable]?: string }>(
  TOGGLE_INSIGHTS_ERRABLE_STATE,
  key => ({
    errable: key,
  })
);

export const toggleInsightsSuccessibleState = createAction<
  IInsightsState,
  { [key in InsightsSuccessible]?: string }
>(TOGGLE_INSIGHTS_SUCCESSIBLE_STATE, key => ({
  successible: key,
}));
//#endregion

/* new-actions-go-here */