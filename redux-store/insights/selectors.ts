import { createSelector } from 'reselect';
import { IStoreState } from '../storeState';
import {
  InsightsErrable,
  InsightsBooleanable,
  InsightsSuccessible,
} from './state';

export const insightsState = () => (state: IStoreState) => state.insights;

//#region Doables
/**
 * Returns true if the evaluation of a booleanable state of a given key(s) is true
 * @param key the key to compare to
 */
export const selectInsightsBooleanableState = (key: InsightsBooleanable | InsightsBooleanable[]) =>
createSelector(
  insightsState(),
  ({ booleanable }) => (Array.isArray(key) ? !!key.filter(k => booleanable[k]).length : booleanable[key])
);

/**
 * Returns the errable state of a given key(s) is true
 * @param key the key to compare to
 */
export const selectInsightsErrableState = (key: InsightsErrable | InsightsErrable[]) =>
createSelector(
  insightsState(),
  ({ errable }) => (Array.isArray(key) ? !!key.filter(k => errable[k]).length : errable[key])
);

/**
 * Returns the successible state of a given key(s) is true
 * @param key the key to compare to
 */
export const selectInsightsSuccessibleState = (key: InsightsSuccessible | InsightsSuccessible[]) =>
createSelector(
  insightsState(),
  ({ successible }) => (Array.isArray(key) ? !!key.filter(k => successible[k]).length : successible[key])
);
//#endregion