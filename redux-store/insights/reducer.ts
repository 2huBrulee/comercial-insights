import { 
  DEFAULT_ACTION,
  RESET_INSIGHTS_DOABLES,
  TOGGLE_INSIGHTS_BOOLEANABLE_STATE,
  TOGGLE_INSIGHTS_ERRABLE_STATE,
  TOGGLE_INSIGHTS_SUCCESSIBLE_STATE,
  /* new-constant-import-goes-here */
} from './constants';

import { IInsightsState } from './state';
import { reducerPayloadDoableHelper } from 'redux-store/rootReducer';

const initialState: IInsightsState = {
  errable: {},
  booleanable: {},
  successible: {},
};

export default (
  state: IInsightsState = initialState,
  { type, payload: incomingPayload }: ReduxActions.Action<IInsightsState>
) => {
  const payload =
    type === RESET_INSIGHTS_DOABLES
      ? incomingPayload
      : (reducerPayloadDoableHelper(state, incomingPayload) as IInsightsState);

  switch (type) {
    case TOGGLE_INSIGHTS_BOOLEANABLE_STATE:
    case TOGGLE_INSIGHTS_ERRABLE_STATE:
    case TOGGLE_INSIGHTS_SUCCESSIBLE_STATE:
    /* new-constant-cases-go-here */
    case DEFAULT_ACTION:
      return {
        ...state, 
        ...payload,
      }
    default:
      return state;
  }
};

