import { all, select, takeLatest, delay, put } from 'redux-saga/effects';
import {
  DEFAULT_ACTION,
/* new-constant-import-goes-here */
} from './constants';
import {
  toggleInsightsBooleanableState
  /* new-action-import-goes-here */
} from './actions';
import { selectInsightsBooleanableState } from './selectors';

export function* intializeSaga() {
  const booleanable = yield select(selectInsightsBooleanableState('__booleanable__'));

  yield put(toggleInsightsBooleanableState({ __booleanable__: !booleanable }));

  console.log('insightsSaga has been initialized properly __booleanable__:', booleanable );
}

/* new-saga-goes-here */

export default function* insightsSaga() {
  yield all([
    takeLatest(DEFAULT_ACTION, intializeSaga),
    /* new-saga-registration-goes-here */
  ]);
}
