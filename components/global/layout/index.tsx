import React from 'react';
import uuid from 'uuid/v4';
import { Breadcrumb, Layout, Menu } from 'antd';
import Link from 'next/link';
import Head from './head';
// import {CustomNProgress} from 'components';
import { compose } from 'recompose';
import { RouterProps, withRouter } from 'next/router';
import '../../../styles/main.scss';

const { Header, Content, Footer } = Layout;
const MenuItem = Menu.Item;

interface Props extends React.HTMLAttributes<any> {
  readonly children?: React.ReactNode;
  readonly description?: string;
  readonly ogImage?: string;
  readonly url?: string;
  readonly router?: RouterProps;
}

const activeClass = 'ant-menu-item-selected';

const MainLayout: React.FC<Props> = ({
                                       title,
                                       description,
                                       ogImage,
                                       url,
                                       router,
                                       children,
                                     }) => {
  const { asPath } = router;

  return (
    <>
      {/* <CustomNProgress /> */}
      <Head title={`challenge | ${title}`} description={description}
            ogImage={ogImage} url={url}/>
      <Layout className="layout">
        <Header>
          <div className="logo"/>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['2']}
            style={{ lineHeight: '64px' }}
          >
            <MenuItem
              key={uuid()}
              className={asPath === '/' ? activeClass : ''}
            >
              <Link href="/">
                <a>Home</a>
              </Link>
            </MenuItem>

            <MenuItem
              key={uuid()}
              className={asPath === '/insights' ? activeClass : ''}
            >
              <Link href="/insights">
                <a>Insights</a>
              </Link>
            </MenuItem>

            {/* new-menu-item */}
          </Menu>
        </Header>
        <Content>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb>
          <div className="content-body">
            {children}
          </div>
        </Content>
        <Footer>
          challenge @{new Date().getFullYear()} Created by Alejandro Merino
        </Footer>
      </Layout>
    </>
  );
};

export default compose<Props, Props>(withRouter)(MainLayout);
