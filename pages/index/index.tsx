import React from 'react';
import Layout from 'components/global/layout';
import './styles.scss';
import { PieChart } from 'components';
import { Col, Row } from 'antd';

export const Home = () => (
  <Layout title="Dashboard" description="Este es el dashboard">
    <p className="home-page">Dashboard !!</p>
    <Row>
      <Col xs={24} md={12}>
        <h3>Numero de Ventas x Ingresante</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Personas que entran un local luego de transitarlo</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Areas mas visitadas</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Areas con mas ventas</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Hora de las ventas</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Fechas (dia de la semana?) con mas ventas</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Local de una marca con cantidad de ingresantes y ventas</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Cuantos likes tiene tal marca</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Visitas por mail y venue</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Local tuvo X cantidad de visitantes y Y likes</h3>
        <PieChart/>
      </Col>
      <Col xs={24} md={12}>
        <h3>Cantidad de Hombres y mujeres</h3>
        <PieChart/>
      </Col>
    </Row>
  </Layout>
);

export default Home;
